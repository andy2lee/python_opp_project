num = -1
while num % 2 is 0 or num == -1 or num < 1:
    num = int(input("Please enter odd number "))

center = 1
for x in range(num // 2, 0, -1):
    for y in range(x):
        print(" ", end="")
    for z in range(center):
        print("*", end="")
    center += 2
    print()
for x_num in range(num):
    print("*", end="")
print()
center -= 2
for x in range(1, num // 2 + 1):
    for y in range(x):
        print(" ", end="")
    for z in range(center):
        print("*", end="")
    center -= 2
    print()
