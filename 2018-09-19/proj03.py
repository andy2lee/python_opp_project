input_num = int(input("salary_low "))
input_hr = float(input("input_hrs "))

if input_hr < 40:
    print(input_num * 0.8 * input_hr, "$")
elif input_hr == 40:
    print(120.0 * 40, "$")
elif input_hr <= 50:
    print(120.0 * 40 + (input_hr - 40) * input_num * 1.2, "$")
else:
    print(120.0 * 40 + (input_hr - 50) * input_num * 1.6, "$")
